  0.00	0	0	U	0	unclassified
100.00	748942	10	-	1	root
100.00	748920	26	-	131567	  cellular organisms
 99.92	748341	0	D	2759	    Eukaryota
 99.92	748341	0	-	33154	      Opisthokonta
 99.92	748341	0	K	33208	        Metazoa
 99.92	748341	0	-	6072	          Eumetazoa
 99.92	748341	0	-	33213	            Bilateria
 99.92	748341	0	-	33511	              Deuterostomia
 99.92	748341	0	P	7711	                Chordata
 99.92	748341	0	-	89593	                  Craniata
 99.92	748341	0	-	7742	                    Vertebrata
 99.92	748341	0	-	7776	                      Gnathostomata
 99.92	748341	0	-	117570	                        Teleostomi
 99.92	748341	0	-	117571	                          Euteleostomi
 99.92	748341	0	-	8287	                            Sarcopterygii
 99.92	748341	0	-	1338369	                              Dipnotetrapodomorpha
 99.92	748341	0	-	32523	                                Tetrapoda
 99.92	748341	0	-	32524	                                  Amniota
 99.92	748341	0	C	40674	                                    Mammalia
 99.92	748341	0	-	32525	                                      Theria
 99.92	748341	0	-	9347	                                        Eutheria
 99.92	748341	0	-	1437010	                                          Boreoeutheria
 99.92	748341	0	-	314146	                                            Euarchontoglires
 99.92	748341	0	O	9443	                                              Primates
 99.92	748341	0	-	376913	                                                Haplorrhini
 99.92	748341	0	-	314293	                                                  Simiiformes
 99.92	748341	0	-	9526	                                                    Catarrhini
 99.92	748341	0	-	314295	                                                      Hominoidea
 99.92	748341	0	F	9604	                                                        Hominidae
 99.92	748341	0	-	207598	                                                          Homininae
 99.92	748341	0	G	9605	                                                            Homo
 99.92	748341	748341	S	9606	                                                              Homo sapiens
  0.07	543	2	D	2	    Bacteria
  0.07	494	0	-	1783272	      Terrabacteria group
  0.06	478	0	P	1239	        Firmicutes
  0.06	473	0	C	91061	          Bacilli
  0.06	467	1	O	1385	            Bacillales
  0.06	443	0	F	186817	              Bacillaceae
  0.06	443	8	G	1386	                Bacillus
  0.06	434	160	S	86661	                  Bacillus cereus group
  0.02	175	151	S	1396	                    Bacillus cereus
  0.00	17	17	-	405532	                      Bacillus cereus B4264
  0.00	3	3	-	526977	                      Bacillus cereus ATCC 4342
  0.00	2	2	-	226900	                      Bacillus cereus ATCC 14579
  0.00	1	1	-	222523	                      Bacillus cereus ATCC 10987
  0.00	1	1	-	1217984	                      Bacillus cereus FRI-35
  0.01	96	63	S	1428	                    Bacillus thuringiensis
  0.00	27	27	-	1442	                      Bacillus thuringiensis serovar tolworthi
  0.00	2	2	-	29338	                      Bacillus thuringiensis serovar galleriae
  0.00	1	0	-	29339	                      Bacillus thuringiensis serovar kurstaki
  0.00	1	1	-	1261129	                        Bacillus thuringiensis serovar kurstaki str. HD-1
  0.00	1	1	-	180850	                      Bacillus thuringiensis serovar indiana
  0.00	1	1	-	412694	                      Bacillus thuringiensis str. Al Hakam
  0.00	1	1	-	529122	                      Bacillus thuringiensis YBT-1518
  0.00	1	1	S	1392	                    Bacillus anthracis
  0.00	1	1	S	1405	                    Bacillus mycoides
  0.00	1	1	S	86662	                    Bacillus weihenstephanensis
  0.00	1	0	S	653685	                  Bacillus subtilis group
  0.00	1	0	S	1938374	                    Bacillus amyloliquefaciens group
  0.00	1	1	S	492670	                      Bacillus velezensis
  0.00	21	0	F	90964	              Staphylococcaceae
  0.00	21	0	G	1279	                Staphylococcus
  0.00	21	15	S	1280	                  Staphylococcus aureus
  0.00	6	1	-	46170	                    Staphylococcus aureus subsp. aureus
  0.00	5	5	-	548473	                      Staphylococcus aureus subsp. aureus TCH60
  0.00	2	0	F	186820	              Listeriaceae
  0.00	2	0	G	1637	                Listeria
  0.00	1	0	S	1638	                  Listeria ivanovii
  0.00	1	1	-	202752	                    Listeria ivanovii subsp. londoniensis
  0.00	1	1	S	1639	                  Listeria monocytogenes
  0.00	6	0	O	186826	            Lactobacillales
  0.00	3	0	F	1300	              Streptococcaceae
  0.00	3	0	G	1301	                Streptococcus
  0.00	1	1	S	28037	                  Streptococcus mitis
  0.00	1	0	S	59310	                  Streptococcus macedonicus
  0.00	1	1	-	1116231	                    Streptococcus macedonicus ACA-DC 198
  0.00	1	0	S	671232	                  Streptococcus anginosus group
  0.00	1	0	S	1338	                    Streptococcus intermedius
  0.00	1	1	-	862967	                      Streptococcus intermedius B196
  0.00	2	0	F	81850	              Leuconostocaceae
  0.00	1	0	G	1243	                Leuconostoc
  0.00	1	1	S	1245	                  Leuconostoc mesenteroides
  0.00	1	0	G	46255	                Weissella
  0.00	1	1	S	137591	                  Weissella cibaria
  0.00	1	0	F	33958	              Lactobacillaceae
  0.00	1	0	G	1578	                Lactobacillus
  0.00	1	1	S	1590	                  Lactobacillus plantarum
  0.00	5	0	C	186801	          Clostridia
  0.00	4	0	O	186802	            Clostridiales
  0.00	3	0	F	31979	              Clostridiaceae
  0.00	3	0	G	1485	                Clostridium
  0.00	1	1	S	1491	                  Clostridium botulinum
  0.00	1	0	S	1501	                  Clostridium pasteurianum
  0.00	1	1	-	1428454	                    Clostridium pasteurianum NRRL B-598
  0.00	1	0	S	1561	                  Clostridium baratii
  0.00	1	1	-	1415775	                    Clostridium baratii str. Sullivan
  0.00	1	0	F	186807	              Peptococcaceae
  0.00	1	1	G	56112	                Dehalobacter
  0.00	1	0	O	68295	            Thermoanaerobacterales
  0.00	1	0	F	543371	              Thermoanaerobacterales Family III. Incertae Sedis
  0.00	1	0	G	44000	                Caldicellulosiruptor
  0.00	1	0	S	717609	                  Caldicellulosiruptor obsidiansis
  0.00	1	1	-	608506	                    Caldicellulosiruptor obsidiansis OB47
  0.00	5	0	P	544448	        Tenericutes
  0.00	5	0	C	31969	          Mollicutes
  0.00	4	0	O	2085	            Mycoplasmatales
  0.00	4	0	F	2092	              Mycoplasmataceae
  0.00	4	0	G	2093	                Mycoplasma
  0.00	1	0	S	2109	                  Mycoplasma synoviae
  0.00	1	1	-	1267001	                    Mycoplasma synoviae ATCC 25204
  0.00	1	0	S	2123	                  Mycoplasma putrefaciens
  0.00	1	1	-	1292033	                    Mycoplasma putrefaciens Mput9231
  0.00	1	0	S	2128	                  Mycoplasma flocculare
  0.00	1	1	-	743971	                    Mycoplasma flocculare ATCC 27399
  0.00	1	1	S	28903	                  Mycoplasma bovis
  0.00	1	0	O	186328	            Entomoplasmatales
  0.00	1	0	F	2131	              Spiroplasmataceae
  0.00	1	0	G	2132	                Spiroplasma
  0.00	1	0	S	2145	                  Spiroplasma taiwanense
  0.00	1	1	-	1276220	                    Spiroplasma taiwanense CT-1
  0.00	5	0	-	1798711	        Cyanobacteria/Melainabacteria group
  0.00	5	0	P	1117	          Cyanobacteria
  0.00	3	0	-	1301283	            Oscillatoriophycideae
  0.00	2	0	O	1150	              Oscillatoriales
  0.00	2	0	F	1892254	                Oscillatoriaceae
  0.00	2	0	G	1158	                  Oscillatoria
  0.00	2	0	S	482564	                    Oscillatoria nigro-viridis
  0.00	2	2	-	179408	                      Oscillatoria nigro-viridis PCC 7112
  0.00	1	0	O	1118	              Chroococcales
  0.00	1	0	F	1890449	                Microcystaceae
  0.00	1	0	G	1125	                  Microcystis
  0.00	1	1	S	1126	                    Microcystis aeruginosa
  0.00	2	0	O	1161	            Nostocales
  0.00	2	0	F	1162	              Nostocaceae
  0.00	2	0	G	1163	                Anabaena
  0.00	2	2	S	46234	                  Anabaena sp. 90
  0.00	4	0	P	201174	        Actinobacteria
  0.00	4	0	C	1760	          Actinobacteria
  0.00	1	0	O	85006	            Micrococcales
  0.00	1	0	F	85017	              Promicromonosporaceae
  0.00	1	0	G	186188	                Xylanimonas
  0.00	1	0	S	186189	                  Xylanimonas cellulosilytica
  0.00	1	1	-	446471	                    Xylanimonas cellulosilytica DSM 15894
  0.00	1	0	O	85007	            Corynebacteriales
  0.00	1	0	F	1653	              Corynebacteriaceae
  0.00	1	0	G	1716	                Corynebacterium
  0.00	1	1	S	571915	                  Corynebacterium mustelae
  0.00	1	0	O	85011	            Streptomycetales
  0.00	1	0	F	2062	              Streptomycetaceae
  0.00	1	0	G	1883	                Streptomyces
  0.00	1	1	S	54571	                  Streptomyces venezuelae
  0.00	1	0	O	85013	            Frankiales
  0.00	1	0	F	74712	              Frankiaceae
  0.00	1	0	G	1854	                Frankia
  0.00	1	0	S	1859	                  Frankia alni
  0.00	1	1	-	326424	                    Frankia alni ACN14a
  0.00	1	0	P	1297	        Deinococcus-Thermus
  0.00	1	0	C	188787	          Deinococci
  0.00	1	0	O	118964	            Deinococcales
  0.00	1	0	F	183710	              Deinococcaceae
  0.00	1	0	G	1298	                Deinococcus
  0.00	1	0	S	502394	                  Deinococcus gobiensis
  0.00	1	1	-	745776	                    Deinococcus gobiensis I-0
  0.00	1	0	P	200795	        Chloroflexi
  0.00	1	0	C	301297	          Dehalococcoidia
  0.00	1	0	O	1202465	            Dehalococcoidales
  0.00	1	0	F	1202464	              Dehalococcoidaceae
  0.00	1	0	G	61434	                Dehalococcoides
  0.00	1	0	S	61435	                  Dehalococcoides mccartyi
  0.00	1	1	-	216389	                    Dehalococcoides mccartyi BAV1
  0.01	40	1	P	1224	      Proteobacteria
  0.00	21	0	C	1236	        Gammaproteobacteria
  0.00	5	0	O	91347	          Enterobacterales
  0.00	3	1	F	543	            Enterobacteriaceae
  0.00	2	0	G	561	              Escherichia
  0.00	2	2	S	562	                Escherichia coli
  0.00	1	0	F	1903409	            Erwiniaceae
  0.00	1	0	G	53335	              Pantoea
  0.00	1	0	S	1654067	                Pantoea agglomerans group
  0.00	1	1	S	549	                  Pantoea agglomerans
  0.00	1	0	F	1903414	            Morganellaceae
  0.00	1	0	G	583	              Proteus
  0.00	1	0	S	584	                Proteus mirabilis
  0.00	1	1	-	529507	                  Proteus mirabilis HI4320
  0.00	4	0	O	135614	          Xanthomonadales
  0.00	4	0	F	32033	            Xanthomonadaceae
  0.00	4	1	G	338	              Xanthomonas
  0.00	2	0	S	643453	                Xanthomonas citri group
  0.00	2	0	S	346	                  Xanthomonas citri
  0.00	1	1	-	473422	                    Xanthomonas citri pv. malvacearum
  0.00	1	1	-	611301	                    Xanthomonas citri subsp. citri
  0.00	1	0	S	347	                Xanthomonas oryzae
  0.00	1	1	-	64187	                  Xanthomonas oryzae pv. oryzae
  0.00	3	0	O	72274	          Pseudomonadales
  0.00	3	0	F	135621	            Pseudomonadaceae
  0.00	3	1	G	286	              Pseudomonas
  0.00	1	0	S	136841	                Pseudomonas aeruginosa group
  0.00	1	0	S	53412	                  Pseudomonas resinovorans
  0.00	1	1	-	1245471	                    Pseudomonas resinovorans NBRC 106553
  0.00	1	0	S	136845	                Pseudomonas putida group
  0.00	1	1	S	303	                  Pseudomonas putida
  0.00	3	0	O	135625	          Pasteurellales
  0.00	3	0	F	712	            Pasteurellaceae
  0.00	1	0	G	724	              Haemophilus
  0.00	1	0	S	727	                Haemophilus influenzae
  0.00	1	1	-	866630	                  Haemophilus influenzae F3031
  0.00	1	0	G	745	              Pasteurella
  0.00	1	0	S	747	                Pasteurella multocida
  0.00	1	0	-	44283	                  Pasteurella multocida subsp. multocida
  0.00	1	1	-	1304873	                    Pasteurella multocida subsp. multocida OH4807
  0.00	1	0	G	214906	              Histophilus
  0.00	1	0	S	731	                Histophilus somni
  0.00	1	1	-	228400	                  Haemophilus somnus 2336
  0.00	2	0	O	1706369	          Cellvibrionales
  0.00	2	0	F	1706371	            Cellvibrionaceae
  0.00	1	0	G	316625	              Saccharophagus
  0.00	1	0	S	86304	                Saccharophagus degradans
  0.00	1	1	-	203122	                  Saccharophagus degradans 2-40
  0.00	1	0	G	447467	              Simiduia
  0.00	1	0	S	447471	                Simiduia agarivorans
  0.00	1	1	-	1117647	                  Simiduia agarivorans SA1 = DSM 21679
  0.00	1	0	O	72273	          Thiotrichales
  0.00	1	0	F	34064	            Francisellaceae
  0.00	1	0	G	262	              Francisella
  0.00	1	1	S	28110	                Francisella philomiragia
  0.00	1	0	-	118884	          unclassified Gammaproteobacteria
  0.00	1	0	-	32036	            sulfur-oxidizing symbionts
  0.00	1	0	S	410330	              Calyptogena okutanii thioautotrophic gill symbiont
  0.00	1	1	-	412965	                Candidatus Vesicomyosocius okutanii HA
  0.00	1	0	O	135619	          Oceanospirillales
  0.00	1	0	F	28256	            Halomonadaceae
  0.00	1	0	-	114403	              Zymobacter group
  0.00	1	0	-	114399	                whitefly endosymbionts
  0.00	1	0	G	235572	                  Candidatus Portiera
  0.00	1	1	S	91844	                    Candidatus Portiera aleyrodidarum
  0.00	1	0	O	135623	          Vibrionales
  0.00	1	0	F	641	            Vibrionaceae
  0.00	1	0	G	662	              Vibrio
  0.00	1	0	S	717610	                Vibrio harveyi group
  0.00	1	1	S	663	                  Vibrio alginolyticus
  0.00	7	0	C	28216	        Betaproteobacteria
  0.00	7	0	O	80840	          Burkholderiales
  0.00	5	0	F	119060	            Burkholderiaceae
  0.00	4	0	G	32008	              Burkholderia
  0.00	2	0	S	111527	                pseudomallei group
  0.00	2	1	S	28450	                  Burkholderia pseudomallei
  0.00	1	1	-	441156	                    Burkholderia pseudomallei 7894
  0.00	1	1	S	28095	                Burkholderia gladioli
  0.00	1	0	S	87882	                Burkholderia cepacia complex
  0.00	1	1	S	95486	                  Burkholderia cenocepacia
  0.00	1	1	G	1822464	              Paraburkholderia
  0.00	1	0	F	506	            Alcaligenaceae
  0.00	1	0	G	507	              Alcaligenes
  0.00	1	1	S	511	                Alcaligenes faecalis
  0.00	1	0	F	80864	            Comamonadaceae
  0.00	1	0	G	47420	              Hydrogenophaga
  0.00	1	1	S	1763535	                Hydrogenophaga sp. LPB0072
  0.00	7	0	-	68525	        delta/epsilon subdivisions
  0.00	5	0	C	28221	          Deltaproteobacteria
  0.00	2	0	O	29	            Myxococcales
  0.00	2	0	-	80812	              Sorangiineae
  0.00	2	0	F	49	                Polyangiaceae
  0.00	2	0	G	39643	                  Sorangium
  0.00	2	0	S	56	                    Sorangium cellulosum
  0.00	2	2	-	1254432	                      Sorangium cellulosum So0157-2
  0.00	2	0	O	213115	            Desulfovibrionales
  0.00	2	0	F	194924	              Desulfovibrionaceae
  0.00	2	0	G	872	                Desulfovibrio
  0.00	1	1	S	44742	                  Desulfovibrio fairfieldensis
  0.00	1	1	S	1716143	                  Desulfovibrio sp. J2
  0.00	1	0	O	213481	            Bdellovibrionales
  0.00	1	0	F	213483	              Bdellovibrionaceae
  0.00	1	0	G	958	                Bdellovibrio
  0.00	1	0	S	959	                  Bdellovibrio bacteriovorus
  0.00	1	1	-	1069642	                    Bdellovibrio bacteriovorus str. Tiberius
  0.00	2	0	C	29547	          Epsilonproteobacteria
  0.00	2	0	O	213849	            Campylobacterales
  0.00	2	0	F	72294	              Campylobacteraceae
  0.00	1	0	G	194	                Campylobacter
  0.00	1	0	S	1031542	                  Campylobacter volucris
  0.00	1	1	-	580033	                    Campylobacter volucris LMG 24379
  0.00	1	0	G	57665	                Sulfurospirillum
  0.00	1	0	S	66821	                  Sulfurospirillum multivorans
  0.00	1	1	-	1150621	                    Sulfurospirillum multivorans DSM 12446
  0.00	4	0	C	28211	        Alphaproteobacteria
  0.00	3	0	O	356	          Rhizobiales
  0.00	1	0	F	41294	            Bradyrhizobiaceae
  0.00	1	0	G	374	              Bradyrhizobium
  0.00	1	1	S	1355477	                Bradyrhizobium diazoefficiens
  0.00	1	0	F	45404	            Beijerinckiaceae
  0.00	1	0	G	532	              Beijerinckia
  0.00	1	0	S	533	                Beijerinckia indica
  0.00	1	0	-	31994	                  Beijerinckia indica subsp. indica
  0.00	1	1	-	395963	                    Beijerinckia indica subsp. indica ATCC 9039
  0.00	1	0	F	82115	            Rhizobiaceae
  0.00	1	0	-	227290	              Rhizobium/Agrobacterium group
  0.00	1	1	G	379	                Rhizobium
  0.00	1	0	O	204441	          Rhodospirillales
  0.00	1	0	F	41295	            Rhodospirillaceae
  0.00	1	0	G	13134	              Magnetospirillum
  0.00	1	0	S	55518	                Magnetospirillum gryphiswaldense
  0.00	1	0	-	431944	                  Magnetospirillum gryphiswaldense MSR-1
  0.00	1	1	-	1430440	                    Magnetospirillum gryphiswaldense MSR-1 v2
  0.00	3	0	P	203691	      Spirochaetes
  0.00	3	0	C	203692	        Spirochaetia
  0.00	2	0	O	136	          Spirochaetales
  0.00	1	0	F	137	            Spirochaetaceae
  0.00	1	0	G	157	              Treponema
  0.00	1	0	S	158	                Treponema denticola
  0.00	1	1	-	243275	                  Treponema denticola ATCC 35405
  0.00	1	0	F	1643685	            Borreliaceae
  0.00	1	1	G	64895	              Borreliella
  0.00	1	0	O	1643686	          Brachyspirales
  0.00	1	0	F	143786	            Brachyspiraceae
  0.00	1	0	G	29521	              Brachyspira
  0.00	1	0	S	84377	                Brachyspira intermedia
  0.00	1	1	-	1045858	                  Brachyspira intermedia PWS/A
  0.00	3	0	-	1783270	      FCB group
  0.00	3	0	-	68336	        Bacteroidetes/Chlorobi group
  0.00	3	0	P	976	          Bacteroidetes
  0.00	2	0	C	200643	            Bacteroidia
  0.00	2	0	O	171549	              Bacteroidales
  0.00	1	0	F	171551	                Porphyromonadaceae
  0.00	1	0	G	836	                  Porphyromonas
  0.00	1	0	S	28123	                    Porphyromonas asaccharolytica
  0.00	1	1	-	879243	                      Porphyromonas asaccharolytica DSM 20707
  0.00	1	0	-	333046	                unclassified Bacteroidales
  0.00	1	0	G	511434	                  Candidatus Azobacteroides
  0.00	1	0	S	511435	                    Candidatus Azobacteroides pseudotrichonymphae
  0.00	1	1	-	511995	                      Candidatus Azobacteroides pseudotrichonymphae genomovar. CFP2
  0.00	1	0	C	768503	            Cytophagia
  0.00	1	0	O	768507	              Cytophagales
  0.00	1	0	F	1853232	                Hymenobacteraceae
  0.00	1	0	G	1379908	                  Rufibacter
  0.00	1	1	S	1379909	                    Rufibacter sp. DG15C
  0.00	1	0	P	200918	      Thermotogae
  0.00	1	0	C	188708	        Thermotogae
  0.00	1	0	O	2419	          Thermotogales
  0.00	1	0	F	1643950	            Fervidobacteriaceae
  0.00	1	0	G	2420	              Thermosipho
  0.00	1	0	S	2421	                Thermosipho africanus
  0.00	1	1	-	484019	                  Thermosipho africanus TCF52B
  0.00	10	0	D	2157	    Archaea
  0.00	10	0	P	28890	      Euryarchaeota
  0.00	7	0	C	224756	        Methanomicrobia
  0.00	7	0	O	94695	          Methanosarcinales
  0.00	7	0	F	2206	            Methanosarcinaceae
  0.00	7	0	G	2207	              Methanosarcina
  0.00	4	1	S	2208	                Methanosarcina barkeri
  0.00	2	2	-	1434107	                  Methanosarcina barkeri 3
  0.00	1	1	-	269797	                  Methanosarcina barkeri str. Fusaro
  0.00	1	0	S	2209	                Methanosarcina mazei
  0.00	1	1	-	1434115	                  Methanosarcina mazei SarPi
  0.00	1	1	S	1434100	                Methanosarcina sp. MTP4
  0.00	1	1	S	1434102	                Methanosarcina sp. WH1
  0.00	2	0	C	183939	        Methanococci
  0.00	2	0	O	2182	          Methanococcales
  0.00	2	0	F	2183	            Methanococcaceae
  0.00	1	0	G	2184	              Methanococcus
  0.00	1	0	S	2187	                Methanococcus vannielii
  0.00	1	1	-	406327	                  Methanococcus vannielii SB
  0.00	1	0	G	155862	              Methanothermococcus
  0.00	1	0	S	155863	                Methanothermococcus okinawensis
  0.00	1	1	-	647113	                  Methanothermococcus okinawensis IH1
  0.00	1	0	C	183925	        Methanobacteria
  0.00	1	0	O	2158	          Methanobacteriales
  0.00	1	0	F	2159	            Methanobacteriaceae
  0.00	1	0	G	2316	              Methanosphaera
  0.00	1	0	S	2317	                Methanosphaera stadtmanae
  0.00	1	1	-	339860	                  Methanosphaera stadtmanae DSM 3091
  0.00	12	0	D	10239	  Viruses
  0.00	9	0	-	35237	    dsDNA viruses, no RNA stage
  0.00	2	0	F	10240	      Poxviridae
  0.00	2	0	-	10241	        Chordopoxvirinae
  0.00	2	0	G	10242	          Orthopoxvirus
  0.00	2	2	S	28871	            Taterapox virus
  0.00	2	0	O	28883	      Caudovirales
  0.00	1	0	F	10662	        Myoviridae
  0.00	1	0	-	857473	          Spounavirinae
  0.00	1	0	-	857474	            unclassified Spounavirinae
  0.00	1	1	S	857312	              Brochothrix phage A9
  0.00	1	0	F	10699	        Siphoviridae
  0.00	1	0	G	1623305	          Sk1virus
  0.00	1	1	S	287412	            Lactococcus phage SL4
  0.00	2	0	-	51368	      unclassified dsDNA viruses
  0.00	2	2	S	1349410	        Pandoravirus salinus
  0.00	1	0	F	10442	      Baculoviridae
  0.00	1	0	G	558017	        Betabaculovirus
  0.00	1	0	-	342107	          unclassified Betabaculovirus
  0.00	1	1	S	1675862	            Diatraea saccharalis granulovirus
  0.00	1	0	F	10482	      Polydnaviridae
  0.00	1	0	G	10485	        Bracovirus
  0.00	1	1	S	39640	          Cotesia congregata bracovirus
  0.00	1	0	F	10501	      Phycodnaviridae
  0.00	1	0	-	455363	        unclassified Phycodnaviridae
  0.00	1	1	S	1474867	          Aureococcus anophagefferens virus
  0.00	2	0	-	439488	    ssRNA viruses
  0.00	2	0	-	35301	      ssRNA negative-strand viruses
  0.00	2	0	F	11571	        Bunyaviridae
  0.00	2	0	G	11611	          Tospovirus
  0.00	1	0	-	326176	            unclassified Tospovirus
  0.00	1	1	S	1033976	              Bean necrotic mosaic virus
  0.00	1	0	S	1933298	            Tomato spotted wilt tospovirus
  0.00	1	1	-	11613	              Tomato spotted wilt virus
  0.00	1	0	-	12877	    Satellites
  0.00	1	0	-	198601	      Satellite Nucleic Acids
  0.00	1	0	-	361688	        Single stranded DNA satellites
  0.00	1	0	-	1458186	          Alphasatellites
  0.00	1	0	-	283494	            Begomovirus-associated alphasatellites
  0.00	1	0	-	1231296	              unclassified Begomovirus-associated alphasatellites
  0.00	1	1	S	661505	                Chilli leaf curl Multan alphasatellite
