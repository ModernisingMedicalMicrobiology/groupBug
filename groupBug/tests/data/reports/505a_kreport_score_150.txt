  0.00	0	0	U	0	unclassified
100.00	499962	4	-	1	root
100.00	499952	11	-	131567	  cellular organisms
 99.94	499646	0	D	2759	    Eukaryota
 99.94	499646	0	-	33154	      Opisthokonta
 99.94	499646	0	K	33208	        Metazoa
 99.94	499646	0	-	6072	          Eumetazoa
 99.94	499646	0	-	33213	            Bilateria
 99.94	499646	0	-	33511	              Deuterostomia
 99.94	499646	0	P	7711	                Chordata
 99.94	499646	0	-	89593	                  Craniata
 99.94	499646	0	-	7742	                    Vertebrata
 99.94	499646	0	-	7776	                      Gnathostomata
 99.94	499646	0	-	117570	                        Teleostomi
 99.94	499646	0	-	117571	                          Euteleostomi
 99.94	499646	0	-	8287	                            Sarcopterygii
 99.94	499646	0	-	1338369	                              Dipnotetrapodomorpha
 99.94	499646	0	-	32523	                                Tetrapoda
 99.94	499646	0	-	32524	                                  Amniota
 99.94	499646	0	C	40674	                                    Mammalia
 99.94	499646	0	-	32525	                                      Theria
 99.94	499646	0	-	9347	                                        Eutheria
 99.94	499646	0	-	1437010	                                          Boreoeutheria
 99.94	499646	0	-	314146	                                            Euarchontoglires
 99.94	499646	0	O	9443	                                              Primates
 99.94	499646	0	-	376913	                                                Haplorrhini
 99.94	499646	0	-	314293	                                                  Simiiformes
 99.94	499646	0	-	9526	                                                    Catarrhini
 99.94	499646	0	-	314295	                                                      Hominoidea
 99.94	499646	0	F	9604	                                                        Hominidae
 99.94	499646	0	-	207598	                                                          Homininae
 99.94	499646	0	G	9605	                                                            Homo
 99.94	499646	499646	S	9606	                                                              Homo sapiens
  0.06	287	1	D	2	    Bacteria
  0.05	236	0	-	1783272	      Terrabacteria group
  0.04	217	0	P	1239	        Firmicutes
  0.04	209	0	C	91061	          Bacilli
  0.04	206	0	O	1385	            Bacillales
  0.04	199	0	F	90964	              Staphylococcaceae
  0.04	199	4	G	1279	                Staphylococcus
  0.04	189	174	S	1282	                  Staphylococcus epidermidis
  0.00	14	14	-	176280	                    Staphylococcus epidermidis ATCC 12228
  0.00	1	1	-	176279	                    Staphylococcus epidermidis RP62A
  0.00	4	2	S	1280	                  Staphylococcus aureus
  0.00	2	1	-	46170	                    Staphylococcus aureus subsp. aureus
  0.00	1	1	-	546342	                      Staphylococcus aureus subsp. aureus str. JKD6008
  0.00	1	0	S	29385	                  Staphylococcus saprophyticus
  0.00	1	0	-	147452	                    Staphylococcus saprophyticus subsp. saprophyticus
  0.00	1	1	-	342451	                      Staphylococcus saprophyticus subsp. saprophyticus ATCC 15305
  0.00	1	1	S	1715860	                  Staphylococcus sp. AntiMn-1
  0.00	7	0	F	186817	              Bacillaceae
  0.00	7	0	G	1386	                Bacillus
  0.00	6	0	S	86661	                  Bacillus cereus group
  0.00	5	4	S	1428	                    Bacillus thuringiensis
  0.00	1	1	-	529122	                      Bacillus thuringiensis YBT-1518
  0.00	1	1	S	1392	                    Bacillus anthracis
  0.00	1	1	S	1892404	                  Bacillus sp. ABP14
  0.00	3	0	O	186826	            Lactobacillales
  0.00	1	0	F	1300	              Streptococcaceae
  0.00	1	0	G	1301	                Streptococcus
  0.00	1	1	S	1308	                  Streptococcus thermophilus
  0.00	1	0	F	81852	              Enterococcaceae
  0.00	1	0	G	1350	                Enterococcus
  0.00	1	1	S	1352	                  Enterococcus faecium
  0.00	1	0	F	186827	              Aerococcaceae
  0.00	1	0	G	1375	                Aerococcus
  0.00	1	1	S	1377	                  Aerococcus viridans
  0.00	8	0	C	186801	          Clostridia
  0.00	7	0	O	186802	            Clostridiales
  0.00	7	0	F	31979	              Clostridiaceae
  0.00	7	0	G	1485	                Clostridium
  0.00	2	2	S	1502	                  Clostridium perfringens
  0.00	1	1	S	1488	                  Clostridium acetobutylicum
  0.00	1	1	S	1491	                  Clostridium botulinum
  0.00	1	0	S	1501	                  Clostridium pasteurianum
  0.00	1	1	-	1428454	                    Clostridium pasteurianum NRRL B-598
  0.00	1	0	S	1520	                  Clostridium beijerinckii
  0.00	1	0	-	290402	                    Clostridium beijerinckii NCIMB 8052
  0.00	1	1	-	864803	                      Clostridium beijerinckii ATCC 35702
  0.00	1	1	S	1548	                  Clostridium scatologenes
  0.00	1	0	O	68295	            Thermoanaerobacterales
  0.00	1	0	F	186814	              Thermoanaerobacteraceae
  0.00	1	0	G	499228	                Tepidanaerobacter
  0.00	1	0	S	499229	                  Tepidanaerobacter acetatoxydans
  0.00	1	1	-	1209989	                    Tepidanaerobacter acetatoxydans Re1
  0.00	12	0	P	201174	        Actinobacteria
  0.00	12	1	C	1760	          Actinobacteria
  0.00	5	0	O	85007	            Corynebacteriales
  0.00	3	0	F	1762	              Mycobacteriaceae
  0.00	3	0	G	1763	                Mycobacterium
  0.00	1	0	S	1800	                  Mycobacterium chubuense
  0.00	1	1	-	710421	                    Mycobacterium chubuense NBB4
  0.00	1	0	S	110539	                  Mycobacterium vanbaalenii
  0.00	1	1	-	350058	                    Mycobacterium vanbaalenii PYR-1
  0.00	1	1	S	1273687	                  Mycobacterium sp. VKM Ac-1817D
  0.00	2	0	F	1653	              Corynebacteriaceae
  0.00	2	0	G	1716	                Corynebacterium
  0.00	1	0	S	203263	                  Corynebacterium aquilae
  0.00	1	1	-	1431546	                    Corynebacterium aquilae DSM 44791
  0.00	1	1	S	571915	                  Corynebacterium mustelae
  0.00	3	0	O	85011	            Streptomycetales
  0.00	3	0	F	2062	              Streptomycetaceae
  0.00	3	0	G	1883	                Streptomyces
  0.00	1	1	S	68570	                  Streptomyces albulus
  0.00	1	1	S	206662	                  Streptomyces sp. FR-008
  0.00	1	0	S	1852274	                  Streptomyces albus group
  0.00	1	1	S	1888	                    Streptomyces albus
  0.00	2	0	O	85006	            Micrococcales
  0.00	2	0	F	85023	              Microbacteriaceae
  0.00	1	0	G	2034	                Curtobacterium
  0.00	1	1	S	1561023	                  Curtobacterium sp. MR_MD2014
  0.00	1	0	G	110932	                Leifsonia
  0.00	1	1	S	1575	                  Leifsonia xyli
  0.00	1	0	O	85008	            Micromonosporales
  0.00	1	0	F	28056	              Micromonosporaceae
  0.00	1	0	G	168694	                Salinispora
  0.00	1	0	S	168697	                  Salinispora arenicola
  0.00	1	1	-	391037	                    Salinispora arenicola CNS-205
  0.00	4	0	-	1798711	        Cyanobacteria/Melainabacteria group
  0.00	4	0	P	1117	          Cyanobacteria
  0.00	3	0	O	1161	            Nostocales
  0.00	2	0	F	1162	              Nostocaceae
  0.00	1	0	G	1163	                Anabaena
  0.00	1	1	S	46234	                  Anabaena sp. 90
  0.00	1	0	G	264688	                Trichormus
  0.00	1	0	S	1164	                  Trichormus azollae
  0.00	1	1	-	551115	                    'Nostoc azollae' 0708
  0.00	1	0	F	1185	              Rivulariaceae
  0.00	1	0	G	1186	                Calothrix
  0.00	1	1	S	1337936	                  Calothrix sp. 336/3
  0.00	1	0	-	34079	            unclassified Cyanobacteria
  0.00	1	0	S	718217	              cyanobacterium endosymbiont of Epithemia turgida
  0.00	1	1	-	1228987	                cyanobacterium endosymbiont of Epithemia turgida isolate EtSB Lake Yunoko
  0.00	3	0	P	544448	        Tenericutes
  0.00	3	0	C	31969	          Mollicutes
  0.00	1	0	O	2085	            Mycoplasmatales
  0.00	1	0	F	2092	              Mycoplasmataceae
  0.00	1	1	G	2093	                Mycoplasma
  0.00	1	0	O	186328	            Entomoplasmatales
  0.00	1	0	F	2131	              Spiroplasmataceae
  0.00	1	0	G	2132	                Spiroplasma
  0.00	1	1	S	216942	                  Spiroplasma litorale
  0.00	1	0	O	186329	            Acholeplasmatales
  0.00	1	0	F	2146	              Acholeplasmataceae
  0.00	1	0	G	33926	                Candidatus Phytoplasma
  0.00	1	0	S	85620	                  Candidatus Phytoplasma asteris
  0.00	1	0	S	100379	                    Onion yellows phytoplasma
  0.00	1	1	-	262768	                      Onion yellows phytoplasma OY-M
  0.01	45	1	P	1224	      Proteobacteria
  0.00	21	0	C	1236	        Gammaproteobacteria
  0.00	6	0	O	91347	          Enterobacterales
  0.00	3	0	F	543	            Enterobacteriaceae
  0.00	1	0	G	547	              Enterobacter
  0.00	1	0	S	354276	                Enterobacter cloacae complex
  0.00	1	0	S	550	                  Enterobacter cloacae
  0.00	1	0	-	336306	                    Enterobacter cloacae subsp. cloacae
  0.00	1	1	-	716541	                      Enterobacter cloacae subsp. cloacae ATCC 13047
  0.00	1	0	G	570	              Klebsiella
  0.00	1	1	S	573	                Klebsiella pneumoniae
  0.00	1	0	G	590	              Salmonella
  0.00	1	0	S	28901	                Salmonella enterica
  0.00	1	0	-	59201	                  Salmonella enterica subsp. enterica
  0.00	1	1	-	90370	                    Salmonella enterica subsp. enterica serovar Typhi
  0.00	2	0	F	1903414	            Morganellaceae
  0.00	1	0	G	626	              Xenorhabdus
  0.00	1	1	S	40576	                Xenorhabdus bovienii
  0.00	1	0	G	29487	              Photorhabdus
  0.00	1	0	S	29488	                Photorhabdus luminescens
  0.00	1	0	-	141679	                  Photorhabdus luminescens subsp. laumondii
  0.00	1	1	-	243265	                    Photorhabdus luminescens subsp. laumondii TTO1
  0.00	1	0	F	1903411	            Yersiniaceae
  0.00	1	0	G	629	              Yersinia
  0.00	1	0	S	1649845	                Yersinia pseudotuberculosis complex
  0.00	1	1	S	367190	                  Yersinia similis
  0.00	5	0	O	72274	          Pseudomonadales
  0.00	4	0	F	135621	            Pseudomonadaceae
  0.00	4	0	G	286	              Pseudomonas
  0.00	1	0	S	136841	                Pseudomonas aeruginosa group
  0.00	1	1	S	287	                  Pseudomonas aeruginosa
  0.00	1	0	S	136843	                Pseudomonas fluorescens group
  0.00	1	1	S	294	                  Pseudomonas fluorescens
  0.00	1	1	S	930166	                Pseudomonas brassicacearum
  0.00	1	1	S	1611770	                Pseudomonas sp. MRSN12121
  0.00	1	0	F	468	            Moraxellaceae
  0.00	1	0	G	469	              Acinetobacter
  0.00	1	1	S	1324350	                Acinetobacter equi
  0.00	4	0	O	135622	          Alteromonadales
  0.00	1	0	F	267888	            Pseudoalteromonadaceae
  0.00	1	0	G	53246	              Pseudoalteromonas
  0.00	1	1	S	43657	                Pseudoalteromonas luteoviolacea
  0.00	1	0	F	267889	            Colwelliaceae
  0.00	1	0	G	28228	              Colwellia
  0.00	1	0	S	28229	                Colwellia psychrerythraea
  0.00	1	1	-	167879	                  Colwellia psychrerythraea 34H
  0.00	1	0	F	267891	            Moritellaceae
  0.00	1	0	G	58050	              Moritella
  0.00	1	1	S	80854	                Moritella viscosa
  0.00	1	0	F	267894	            Psychromonadaceae
  0.00	1	0	G	67572	              Psychromonas
  0.00	1	0	S	357794	                Psychromonas ingrahamii
  0.00	1	1	-	357804	                  Psychromonas ingrahamii 37
  0.00	2	0	O	135614	          Xanthomonadales
  0.00	2	0	F	32033	            Xanthomonadaceae
  0.00	1	0	G	338	              Xanthomonas
  0.00	1	0	S	643453	                Xanthomonas citri group
  0.00	1	0	S	346	                  Xanthomonas citri
  0.00	1	1	-	473422	                    Xanthomonas citri pv. malvacearum
  0.00	1	0	G	2370	              Xylella
  0.00	1	0	S	2371	                Xylella fastidiosa
  0.00	1	0	-	671135	                  Xylella fastidiosa subsp. sandyi
  0.00	1	1	-	155920	                    Xylella fastidiosa subsp. sandyi Ann-1
  0.00	2	0	O	135619	          Oceanospirillales
  0.00	2	0	F	28256	            Halomonadaceae
  0.00	2	0	-	114403	              Zymobacter group
  0.00	2	0	-	114399	                whitefly endosymbionts
  0.00	2	0	G	235572	                  Candidatus Portiera
  0.00	2	0	S	91844	                    Candidatus Portiera aleyrodidarum
  0.00	1	1	-	1163752	                      Candidatus Portiera aleyrodidarum MED (Bemisia tabaci)
  0.00	1	1	-	1239881	                      Candidatus Portiera aleyrodidarum BT-QVLC
  0.00	1	0	O	72273	          Thiotrichales
  0.00	1	0	F	34064	            Francisellaceae
  0.00	1	0	G	262	              Francisella
  0.00	1	1	S	28110	                Francisella philomiragia
  0.00	1	0	O	135623	          Vibrionales
  0.00	1	0	F	641	            Vibrionaceae
  0.00	1	0	G	511678	              Aliivibrio
  0.00	1	1	S	80852	                Aliivibrio wodanis
  0.00	15	0	C	28216	        Betaproteobacteria
  0.00	14	1	O	80840	          Burkholderiales
  0.00	7	0	F	119060	            Burkholderiaceae
  0.00	5	0	G	32008	              Burkholderia
  0.00	2	1	S	28095	                Burkholderia gladioli
  0.00	1	1	-	999541	                  Burkholderia gladioli BSR3
  0.00	2	0	S	111527	                pseudomallei group
  0.00	2	0	S	28450	                  Burkholderia pseudomallei
  0.00	1	1	-	441156	                    Burkholderia pseudomallei 7894
  0.00	1	1	-	1306418	                    Burkholderia pseudomallei HBPUB10134a
  0.00	1	1	S	1740163	                Burkholderia sp. Bp7605
  0.00	1	0	G	48736	              Ralstonia
  0.00	1	1	S	305	                Ralstonia solanacearum
  0.00	1	0	G	106589	              Cupriavidus
  0.00	1	1	S	1389192	                Cupriavidus sp. USMAHM13
  0.00	3	0	F	506	            Alcaligenaceae
  0.00	3	0	G	517	              Bordetella
  0.00	2	1	S	518	                Bordetella bronchiseptica
  0.00	1	1	-	568707	                  Bordetella bronchiseptica 253
  0.00	1	1	S	520	                Bordetella pertussis
  0.00	3	0	F	80864	            Comamonadaceae
  0.00	1	1	G	12916	              Acidovorax
  0.00	1	1	G	80865	              Delftia
  0.00	1	0	G	174951	              Ramlibacter
  0.00	1	0	S	94132	                Ramlibacter tataouinensis
  0.00	1	1	-	365046	                  Ramlibacter tataouinensis TTB310
  0.00	1	0	O	206389	          Rhodocyclales
  0.00	1	0	F	75787	            Rhodocyclaceae
  0.00	1	0	G	12960	              Azoarcus
  0.00	1	1	S	198107	                Azoarcus sp. CIB
  0.00	6	0	C	28211	        Alphaproteobacteria
  0.00	5	0	O	356	          Rhizobiales
  0.00	4	0	F	82115	            Rhizobiaceae
  0.00	2	0	-	227290	              Rhizobium/Agrobacterium group
  0.00	1	1	G	357	                Agrobacterium
  0.00	1	1	G	379	                Rhizobium
  0.00	2	0	-	227292	              Sinorhizobium/Ensifer group
  0.00	2	0	G	28105	                Sinorhizobium
  0.00	1	0	S	110321	                  Sinorhizobium medicae
  0.00	1	1	-	366394	                    Sinorhizobium medicae WSM419
  0.00	1	0	S	194963	                  Sinorhizobium americanum
  0.00	1	1	-	1408224	                    Sinorhizobium americanum CCGM7
  0.00	1	0	F	41294	            Bradyrhizobiaceae
  0.00	1	0	G	374	              Bradyrhizobium
  0.00	1	1	S	1274631	                Bradyrhizobium icense
  0.00	1	0	O	255473	          Parvularculales
  0.00	1	0	F	255474	            Parvularculaceae
  0.00	1	0	G	208215	              Parvularcula
  0.00	1	0	S	208216	                Parvularcula bermudensis
  0.00	1	1	-	314260	                  Parvularcula bermudensis HTCC2503
  0.00	2	0	-	68525	        delta/epsilon subdivisions
  0.00	2	0	C	28221	          Deltaproteobacteria
  0.00	1	0	O	29	            Myxococcales
  0.00	1	0	-	80811	              Cystobacterineae
  0.00	1	0	F	1524215	                Anaeromyxobacteraceae
  0.00	1	0	G	161492	                  Anaeromyxobacter
  0.00	1	1	S	404589	                    Anaeromyxobacter sp. Fw109-5
  0.00	1	0	O	213115	            Desulfovibrionales
  0.00	1	0	F	194924	              Desulfovibrionaceae
  0.00	1	0	G	872	                Desulfovibrio
  0.00	1	1	S	44742	                  Desulfovibrio fairfieldensis
  0.00	4	0	-	1783270	      FCB group
  0.00	4	0	-	68336	        Bacteroidetes/Chlorobi group
  0.00	4	0	P	976	          Bacteroidetes
  0.00	2	0	C	117743	            Flavobacteriia
  0.00	2	0	O	200644	              Flavobacteriales
  0.00	2	0	F	49546	                Flavobacteriaceae
  0.00	1	0	G	28250	                  Ornithobacterium
  0.00	1	0	S	28251	                    Ornithobacterium rhinotracheale
  0.00	1	1	-	867902	                      Ornithobacterium rhinotracheale DSM 15997
  0.00	1	0	G	104264	                  Cellulophaga
  0.00	1	1	S	979	                    Cellulophaga lytica
  0.00	1	0	C	117747	            Sphingobacteriia
  0.00	1	0	O	200666	              Sphingobacteriales
  0.00	1	0	F	84566	                Sphingobacteriaceae
  0.00	1	0	G	28453	                  Sphingobacterium
  0.00	1	1	S	1538644	                    Sphingobacterium sp. ML3W
  0.00	1	0	C	200643	            Bacteroidia
  0.00	1	0	O	171549	              Bacteroidales
  0.00	1	0	F	171552	                Prevotellaceae
  0.00	1	0	G	838	                  Prevotella
  0.00	1	0	S	28132	                    Prevotella melaninogenica
  0.00	1	1	-	553174	                      Prevotella melaninogenica ATCC 25845
  0.00	1	0	P	200918	      Thermotogae
  0.00	1	0	C	188708	        Thermotogae
  0.00	1	0	O	2419	          Thermotogales
  0.00	1	0	F	1643950	            Fervidobacteriaceae
  0.00	1	0	G	2420	              Thermosipho
  0.00	1	0	S	2421	                Thermosipho africanus
  0.00	1	1	-	484019	                  Thermosipho africanus TCF52B
  0.00	8	0	D	2157	    Archaea
  0.00	8	0	P	28890	      Euryarchaeota
  0.00	4	0	C	224756	        Methanomicrobia
  0.00	4	0	O	94695	          Methanosarcinales
  0.00	4	0	F	2206	            Methanosarcinaceae
  0.00	4	0	G	2207	              Methanosarcina
  0.00	2	0	S	2208	                Methanosarcina barkeri
  0.00	2	2	-	1434109	                  Methanosarcina barkeri str. Wiesmoor
  0.00	1	0	S	2209	                Methanosarcina mazei
  0.00	1	1	-	1434115	                  Methanosarcina mazei SarPi
  0.00	1	1	S	1434100	                Methanosarcina sp. MTP4
  0.00	3	0	C	183939	        Methanococci
  0.00	3	0	O	2182	          Methanococcales
  0.00	3	0	F	2183	            Methanococcaceae
  0.00	2	0	G	155862	              Methanothermococcus
  0.00	2	0	S	155863	                Methanothermococcus okinawensis
  0.00	2	2	-	647113	                  Methanothermococcus okinawensis IH1
  0.00	1	0	G	2184	              Methanococcus
  0.00	1	0	S	2187	                Methanococcus vannielii
  0.00	1	1	-	406327	                  Methanococcus vannielii SB
  0.00	1	0	C	183925	        Methanobacteria
  0.00	1	0	O	2158	          Methanobacteriales
  0.00	1	0	F	2159	            Methanobacteriaceae
  0.00	1	0	G	2172	              Methanobrevibacter
  0.00	1	1	S	294671	                Methanobrevibacter olleyae
  0.00	6	0	D	10239	  Viruses
  0.00	5	0	-	35237	    dsDNA viruses, no RNA stage
  0.00	3	0	F	10240	      Poxviridae
  0.00	3	0	-	10241	        Chordopoxvirinae
  0.00	3	0	G	10242	          Orthopoxvirus
  0.00	3	3	S	28871	            Taterapox virus
  0.00	1	0	-	51368	      unclassified dsDNA viruses
  0.00	1	1	S	1349409	        Pandoravirus dulcis
  0.00	1	0	O	548681	      Herpesvirales
  0.00	1	0	F	548682	        Alloherpesviridae
  0.00	1	0	G	692606	          Cyprinivirus
  0.00	1	1	S	317858	            Cyprinid herpesvirus 1
  0.00	1	0	-	35268	    Retro-transcribing viruses
  0.00	1	0	F	11632	      Retroviridae
  0.00	1	0	-	35276	        unclassified Retroviridae
  0.00	1	0	-	206037	          Human endogenous retroviruses
  0.00	1	0	S	45617	            Human endogenous retrovirus K
  0.00	1	1	-	166122	              Human endogenous retrovirus K113
