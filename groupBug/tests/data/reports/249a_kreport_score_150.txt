  0.00	0	0	U	0	unclassified
100.00	442051	16	-	1	root
 99.99	441986	29	-	131567	  cellular organisms
 92.14	407319	0	D	2759	    Eukaryota
 92.14	407319	0	-	33154	      Opisthokonta
 92.14	407319	0	K	33208	        Metazoa
 92.14	407319	0	-	6072	          Eumetazoa
 92.14	407319	0	-	33213	            Bilateria
 92.14	407319	0	-	33511	              Deuterostomia
 92.14	407319	0	P	7711	                Chordata
 92.14	407319	0	-	89593	                  Craniata
 92.14	407319	0	-	7742	                    Vertebrata
 92.14	407319	0	-	7776	                      Gnathostomata
 92.14	407319	0	-	117570	                        Teleostomi
 92.14	407319	0	-	117571	                          Euteleostomi
 92.14	407319	0	-	8287	                            Sarcopterygii
 92.14	407319	0	-	1338369	                              Dipnotetrapodomorpha
 92.14	407319	0	-	32523	                                Tetrapoda
 92.14	407319	0	-	32524	                                  Amniota
 92.14	407319	0	C	40674	                                    Mammalia
 92.14	407319	0	-	32525	                                      Theria
 92.14	407319	0	-	9347	                                        Eutheria
 92.14	407319	0	-	1437010	                                          Boreoeutheria
 92.14	407319	0	-	314146	                                            Euarchontoglires
 92.14	407319	0	O	9443	                                              Primates
 92.14	407319	0	-	376913	                                                Haplorrhini
 92.14	407319	0	-	314293	                                                  Simiiformes
 92.14	407319	0	-	9526	                                                    Catarrhini
 92.14	407319	0	-	314295	                                                      Hominoidea
 92.14	407319	0	F	9604	                                                        Hominidae
 92.14	407319	0	-	207598	                                                          Homininae
 92.14	407319	0	G	9605	                                                            Homo
 92.14	407319	407319	S	9606	                                                              Homo sapiens
  7.83	34625	2	D	2	    Bacteria
  7.81	34520	2	-	1783272	      Terrabacteria group
  7.79	34451	0	P	201174	        Actinobacteria
  7.79	34451	12	C	1760	          Actinobacteria
  7.79	34417	1	O	85009	            Propionibacteriales
  7.79	34416	132	F	31957	              Propionibacteriaceae
  7.72	34126	20	G	1912216	                Cutibacterium
  7.70	34050	20789	S	1747	                  Cutibacterium acnes
  1.58	6991	6991	-	1031709	                    Propionibacterium acnes 6609
  1.13	4979	4979	-	1134454	                    Propionibacterium acnes HL096PA1
  0.23	1027	0	-	1905725	                    Propionibacterium acnes subsp. defendens
  0.23	1027	1027	-	1091045	                      Propionibacterium acnes subsp. defendens ATCC 11828
  0.05	207	81	-	1734925	                    Propionibacterium acnes subsp. acnes
  0.01	54	54	-	1114967	                      Propionibacterium acnes TypeIA2 P.acn17
  0.01	45	45	-	1114966	                      Propionibacterium acnes TypeIA2 P.acn33
  0.01	27	27	-	1114969	                      Propionibacterium acnes TypeIA2 P.acn31
  0.01	38	38	-	553199	                    Propionibacterium acnes SK137
  0.00	11	11	-	1276648	                    Propionibacterium acnes hdn-1
  0.00	8	8	-	909952	                    Propionibacterium acnes 266
  0.01	56	34	S	33010	                  Cutibacterium avidum
  0.00	22	22	-	1170318	                    Cutibacterium avidum 44067
  0.03	145	0	G	1743	                Propionibacterium
  0.03	144	144	S	671223	                  Propionibacterium sp. oral taxon 193
  0.00	1	0	S	1744	                  Propionibacterium freudenreichii
  0.00	1	1	-	1752	                    Propionibacterium freudenreichii subsp. shermanii
  0.00	11	0	G	1912215	                Acidipropionibacterium
  0.00	11	11	S	1748	                  Acidipropionibacterium acidipropionici
  0.00	1	0	G	203133	                Propionimicrobium
  0.00	1	1	S	1871034	                  Propionimicrobium sp. Marseille-P3275
  0.00	1	0	G	1912217	                Pseudopropionibacterium
  0.00	1	0	S	1750	                  Pseudopropionibacterium propionicum
  0.00	1	1	-	767029	                    Pseudopropionibacterium propionicum F0230a
  0.00	10	0	O	85007	            Corynebacteriales
  0.00	5	0	F	1653	              Corynebacteriaceae
  0.00	5	1	G	1716	                Corynebacterium
  0.00	2	0	S	38305	                  Corynebacterium vitaeruminis
  0.00	2	2	-	1224164	                    Corynebacterium vitaeruminis DSM 20294
  0.00	1	0	S	1719	                  Corynebacterium pseudotuberculosis
  0.00	1	1	-	1161911	                    Corynebacterium pseudotuberculosis Cp162
  0.00	1	1	S	161896	                  Corynebacterium camporealensis
  0.00	4	0	F	1762	              Mycobacteriaceae
  0.00	4	0	G	1763	                Mycobacterium
  0.00	1	1	S	1769	                  Mycobacterium leprae
  0.00	1	0	S	1781	                  Mycobacterium marinum
  0.00	1	1	-	216594	                    Mycobacterium marinum M
  0.00	1	1	S	164757	                  Mycobacterium sp. JLS
  0.00	1	0	S	670516	                  Mycobacterium chelonae group
  0.00	1	0	S	670506	                    Mycobacterium abscessus subgroup
  0.00	1	0	S	36809	                      Mycobacterium abscessus
  0.00	1	0	-	319705	                        Mycobacterium abscessus subsp. bolletii
  0.00	1	1	-	1303024	                          Mycobacterium abscessus subsp. bolletii 50594
  0.00	1	0	F	85026	              Gordoniaceae
  0.00	1	0	G	2053	                Gordonia
  0.00	1	1	S	337191	                  Gordonia sp. KTR9
  0.00	7	1	O	85006	            Micrococcales
  0.00	2	0	F	85023	              Microbacteriaceae
  0.00	1	0	G	2034	                Curtobacterium
  0.00	1	1	S	1561023	                  Curtobacterium sp. MR_MD2014
  0.00	1	0	G	33882	                Microbacterium
  0.00	1	1	S	1795053	                  Microbacterium sp. PAMC 28756
  0.00	1	0	F	1268	              Micrococcaceae
  0.00	1	0	G	1663	                Arthrobacter
  0.00	1	1	S	656366	                  Arthrobacter alpinus
  0.00	1	0	F	85019	              Brevibacteriaceae
  0.00	1	0	G	1696	                Brevibacterium
  0.00	1	1	S	1703	                  Brevibacterium linens
  0.00	1	0	F	145357	              Dermacoccaceae
  0.00	1	0	G	57499	                Kytococcus
  0.00	1	0	S	1276	                  Kytococcus sedentarius
  0.00	1	1	-	478801	                    Kytococcus sedentarius DSM 20547
  0.00	1	0	-	577468	              unclassified Micrococcales
  0.00	1	0	G	2038	                Tropheryma
  0.00	1	0	S	2039	                  Tropheryma whipplei
  0.00	1	1	-	218496	                    Tropheryma whipplei TW08/27
  0.00	2	0	O	2037	            Actinomycetales
  0.00	2	0	F	2049	              Actinomycetaceae
  0.00	2	0	G	1654	                Actinomyces
  0.00	1	1	S	52773	                  Actinomyces meyeri
  0.00	1	1	S	111015	                  Actinomyces radicidentis
  0.00	1	0	O	85010	            Pseudonocardiales
  0.00	1	0	F	2070	              Pseudonocardiaceae
  0.00	1	0	G	1813	                Amycolatopsis
  0.00	1	0	S	1814	                  Amycolatopsis methanolica
  0.00	1	1	-	1068978	                    Amycolatopsis methanolica 239
  0.00	1	0	O	85011	            Streptomycetales
  0.00	1	0	F	2062	              Streptomycetaceae
  0.00	1	1	G	1883	                Streptomyces
  0.00	1	0	O	85013	            Frankiales
  0.00	1	0	F	74712	              Frankiaceae
  0.00	1	0	G	1854	                Frankia
  0.00	1	1	S	106370	                  Frankia casuarinae
  0.01	54	0	P	1239	        Firmicutes
  0.01	49	0	C	91061	          Bacilli
  0.01	44	0	O	1385	            Bacillales
  0.01	43	0	F	186817	              Bacillaceae
  0.01	43	0	G	1386	                Bacillus
  0.01	42	0	S	86661	                  Bacillus cereus group
  0.01	41	39	S	1428	                    Bacillus thuringiensis
  0.00	2	2	-	412694	                      Bacillus thuringiensis str. Al Hakam
  0.00	1	0	S	86662	                    Bacillus weihenstephanensis
  0.00	1	1	-	315730	                      Bacillus weihenstephanensis KBAB4
  0.00	1	0	S	653685	                  Bacillus subtilis group
  0.00	1	0	S	1938374	                    Bacillus amyloliquefaciens group
  0.00	1	1	S	1390	                      Bacillus amyloliquefaciens
  0.00	1	0	F	186822	              Paenibacillaceae
  0.00	1	0	G	44249	                Paenibacillus
  0.00	1	1	S	1536771	                  Paenibacillus sp. FSL R5-0912
  0.00	5	0	O	186826	            Lactobacillales
  0.00	2	0	F	1300	              Streptococcaceae
  0.00	1	0	G	1301	                Streptococcus
  0.00	1	0	S	671232	                  Streptococcus anginosus group
  0.00	1	0	S	1328	                    Streptococcus anginosus
  0.00	1	0	-	1272910	                      Streptococcus anginosus subsp. whileyi
  0.00	1	1	-	1353243	                        Streptococcus anginosus subsp. whileyi MAS624
  0.00	1	0	G	1357	                Lactococcus
  0.00	1	0	S	1358	                  Lactococcus lactis
  0.00	1	0	-	1359	                    Lactococcus lactis subsp. cremoris
  0.00	1	1	-	1104322	                      Lactococcus lactis subsp. cremoris A76
  0.00	2	0	F	81850	              Leuconostocaceae
  0.00	1	0	G	46254	                Oenococcus
  0.00	1	0	S	1247	                  Oenococcus oeni
  0.00	1	1	-	203123	                    Oenococcus oeni PSU-1
  0.00	1	0	G	46255	                Weissella
  0.00	1	1	S	759620	                  Weissella ceti
  0.00	1	0	F	33958	              Lactobacillaceae
  0.00	1	0	G	1578	                Lactobacillus
  0.00	1	1	S	28038	                  Lactobacillus curvatus
  0.00	4	0	C	186801	          Clostridia
  0.00	4	0	O	186802	            Clostridiales
  0.00	2	0	F	31979	              Clostridiaceae
  0.00	2	0	G	1485	                Clostridium
  0.00	1	1	S	1488	                  Clostridium acetobutylicum
  0.00	1	1	S	1216932	                  Clostridium bornimense
  0.00	1	0	F	186803	              Lachnospiraceae
  0.00	1	0	G	572511	                Blautia
  0.00	1	1	S	1796616	                  Blautia sp. YL58
  0.00	1	0	F	186804	              Peptostreptococcaceae
  0.00	1	0	G	44259	                Filifactor
  0.00	1	0	S	143361	                  Filifactor alocis
  0.00	1	1	-	546269	                    Filifactor alocis ATCC 35896
  0.00	1	0	C	1737404	          Tissierellia
  0.00	1	0	O	1737405	            Tissierellales
  0.00	1	0	F	1570339	              Peptoniphilaceae
  0.00	1	0	G	543311	                Parvimonas
  0.00	1	0	S	33033	                  Parvimonas micra
  0.00	1	1	-	1408286	                    Parvimonas micra A293
  0.00	7	0	P	544448	        Tenericutes
  0.00	7	0	C	31969	          Mollicutes
  0.00	5	0	O	2085	            Mycoplasmatales
  0.00	5	0	F	2092	              Mycoplasmataceae
  0.00	5	0	G	2093	                Mycoplasma
  0.00	2	0	S	57372	                  Mycoplasma suis
  0.00	1	1	-	708248	                    Mycoplasma suis KI3806
  0.00	1	1	-	768700	                    Mycoplasma suis str. Illinois
  0.00	1	1	S	2098	                  Mycoplasma hominis
  0.00	1	0	S	2099	                  Mycoplasma hyopneumoniae
  0.00	1	1	-	754503	                    Mycoplasma hyopneumoniae 7422
  0.00	1	0	S	29501	                  Mycoplasma haemofelis
  0.00	1	1	-	941640	                    Mycoplasma haemofelis str. Langford 1
  0.00	1	0	O	186328	            Entomoplasmatales
  0.00	1	0	F	2131	              Spiroplasmataceae
  0.00	1	0	G	2132	                Spiroplasma
  0.00	1	1	S	216938	                  Spiroplasma helicoides
  0.00	1	0	O	186329	            Acholeplasmatales
  0.00	1	0	F	2146	              Acholeplasmataceae
  0.00	1	0	G	33926	                Candidatus Phytoplasma
  0.00	1	0	S	85632	                  16SrXII (Stolbur group)
  0.00	1	1	S	59748	                    Candidatus Phytoplasma australiense
  0.00	5	0	-	1798711	        Cyanobacteria/Melainabacteria group
  0.00	5	0	P	1117	          Cyanobacteria
  0.00	4	0	O	1161	            Nostocales
  0.00	4	0	F	1162	              Nostocaceae
  0.00	4	0	G	1163	                Anabaena
  0.00	3	3	S	46234	                  Anabaena sp. 90
  0.00	1	1	S	1647413	                  Anabaena sp. wa102
  0.00	1	0	-	1301283	            Oscillatoriophycideae
  0.00	1	0	O	1150	              Oscillatoriales
  0.00	1	0	F	1892254	                Oscillatoriaceae
  0.00	1	0	G	1158	                  Oscillatoria
  0.00	1	0	S	118323	                    Oscillatoria acuminata
  0.00	1	1	-	56110	                      Oscillatoria acuminata PCC 6304
  0.00	1	0	P	1297	        Deinococcus-Thermus
  0.00	1	0	C	188787	          Deinococci
  0.00	1	0	O	68933	            Thermales
  0.00	1	0	F	188786	              Thermaceae
  0.00	1	0	G	270	                Thermus
  0.00	1	0	S	56957	                  Thermus oshimai
  0.00	1	1	-	751945	                    Thermus oshimai JL-2
  0.02	84	2	P	1224	      Proteobacteria
  0.01	47	0	C	1236	        Gammaproteobacteria
  0.00	15	0	O	91347	          Enterobacterales
  0.00	8	0	F	543	            Enterobacteriaceae
  0.00	2	0	G	547	              Enterobacter
  0.00	2	2	S	1868135	                Enterobacter sp. HK169
  0.00	1	0	G	561	              Escherichia
  0.00	1	1	S	562	                Escherichia coli
  0.00	1	0	G	570	              Klebsiella
  0.00	1	1	S	573	                Klebsiella pneumoniae
  0.00	1	0	G	590	              Salmonella
  0.00	1	0	S	28901	                Salmonella enterica
  0.00	1	0	-	59201	                  Salmonella enterica subsp. enterica
  0.00	1	0	-	149539	                    Salmonella enterica subsp. enterica serovar Enteritidis
  0.00	1	1	-	1412470	                      Salmonella enterica subsp. enterica serovar Enteritidis str. EC20120008
  0.00	1	0	G	160674	              Raoultella
  0.00	1	0	S	54291	                Raoultella ornithinolytica
  0.00	1	1	-	1286170	                  Raoultella ornithinolytica B6
  0.00	1	0	G	413496	              Cronobacter
  0.00	1	0	S	413501	                Cronobacter muytjensii
  0.00	1	1	-	1159613	                  Cronobacter muytjensii ATCC 51329
  0.00	1	0	G	1330546	              Pluralibacter
  0.00	1	1	S	61647	                Pluralibacter gergoviae
  0.00	6	0	F	1903414	            Morganellaceae
  0.00	5	0	G	29487	              Photorhabdus
  0.00	5	0	S	29488	                Photorhabdus luminescens
  0.00	5	0	-	141679	                  Photorhabdus luminescens subsp. laumondii
  0.00	5	5	-	243265	                    Photorhabdus luminescens subsp. laumondii TTO1
  0.00	1	0	G	583	              Proteus
  0.00	1	0	S	584	                Proteus mirabilis
  0.00	1	1	-	1266738	                  Proteus mirabilis BB2000
  0.00	1	0	-	451511	            unclassified Enterobacterales
  0.00	1	0	G	702	              Plesiomonas
  0.00	1	1	S	703	                Plesiomonas shigelloides
  0.00	10	0	O	72274	          Pseudomonadales
  0.00	9	0	F	135621	            Pseudomonadaceae
  0.00	9	2	G	286	              Pseudomonas
  0.00	3	0	S	136841	                Pseudomonas aeruginosa group
  0.00	3	2	S	287	                  Pseudomonas aeruginosa
  0.00	1	1	-	1427342	                    Pseudomonas aeruginosa SCV20265
  0.00	1	1	S	69328	                Pseudomonas sp. VLB120
  0.00	1	0	S	136843	                Pseudomonas fluorescens group
  0.00	1	0	S	294	                  Pseudomonas fluorescens
  0.00	1	1	-	1221522	                    Pseudomonas fluorescens NCIMB 11764
  0.00	1	1	S	136845	                Pseudomonas putida group
  0.00	1	1	S	1659194	                Pseudomonas sp. GR 6-02
  0.00	1	0	F	468	            Moraxellaceae
  0.00	1	0	G	469	              Acinetobacter
  0.00	1	1	S	487316	                Acinetobacter soli
  0.00	10	0	O	135622	          Alteromonadales
  0.00	6	0	F	267891	            Moritellaceae
  0.00	6	0	G	58050	              Moritella
  0.00	6	6	S	80854	                Moritella viscosa
  0.00	3	0	F	267888	            Pseudoalteromonadaceae
  0.00	3	0	G	53246	              Pseudoalteromonas
  0.00	3	3	S	43657	                Pseudoalteromonas luteoviolacea
  0.00	1	0	F	267894	            Psychromonadaceae
  0.00	1	0	G	67572	              Psychromonas
  0.00	1	1	S	314282	                Psychromonas sp. CNPT3
  0.00	8	0	O	135619	          Oceanospirillales
  0.00	8	0	F	28256	            Halomonadaceae
  0.00	8	0	-	114403	              Zymobacter group
  0.00	8	0	-	114399	                whitefly endosymbionts
  0.00	8	0	G	235572	                  Candidatus Portiera
  0.00	8	1	S	91844	                    Candidatus Portiera aleyrodidarum
  0.00	4	4	-	1239881	                      Candidatus Portiera aleyrodidarum BT-QVLC
  0.00	3	3	-	1163752	                      Candidatus Portiera aleyrodidarum MED (Bemisia tabaci)
  0.00	2	0	O	135625	          Pasteurellales
  0.00	2	0	F	712	            Pasteurellaceae
  0.00	2	0	G	724	              Haemophilus
  0.00	2	2	S	727	                Haemophilus influenzae
  0.00	1	0	O	135614	          Xanthomonadales
  0.00	1	0	F	32033	            Xanthomonadaceae
  0.00	1	0	G	338	              Xanthomonas
  0.00	1	0	S	339	                Xanthomonas campestris
  0.00	1	1	-	340	                  Xanthomonas campestris pv. campestris
  0.00	1	0	O	135624	          Aeromonadales
  0.00	1	0	F	84642	            Aeromonadaceae
  0.00	1	0	G	642	              Aeromonas
  0.00	1	0	S	645	                Aeromonas salmonicida
  0.00	1	0	-	29491	                  Aeromonas salmonicida subsp. salmonicida
  0.00	1	1	-	382245	                    Aeromonas salmonicida subsp. salmonicida A449
  0.00	17	0	C	28216	        Betaproteobacteria
  0.00	16	0	O	80840	          Burkholderiales
  0.00	8	0	F	119060	            Burkholderiaceae
  0.00	8	1	G	32008	              Burkholderia
  0.00	4	0	S	111527	                pseudomallei group
  0.00	2	2	S	28450	                  Burkholderia pseudomallei
  0.00	1	1	S	13373	                  Burkholderia mallei
  0.00	1	1	S	57975	                  Burkholderia thailandensis
  0.00	1	0	S	337	                Burkholderia glumae
  0.00	1	1	-	1176492	                  Burkholderia glumae LMG 2196 = ATCC 33617
  0.00	1	0	S	87882	                Burkholderia cepacia complex
  0.00	1	0	S	60552	                  Burkholderia vietnamiensis
  0.00	1	1	-	1449978	                    Burkholderia vietnamiensis LMG 10929
  0.00	1	1	S	758796	                Burkholderia sp. RPE67
  0.00	4	0	F	80864	            Comamonadaceae
  0.00	1	0	G	12916	              Acidovorax
  0.00	1	1	S	232721	                Acidovorax sp. JS42
  0.00	1	0	G	47420	              Hydrogenophaga
  0.00	1	1	S	1763535	                Hydrogenophaga sp. LPB0072
  0.00	1	0	G	80865	              Delftia
  0.00	1	1	S	742013	                Delftia sp. Cs1-4
  0.00	1	0	G	364316	              Verminephrobacter
  0.00	1	0	S	364317	                Verminephrobacter eiseniae
  0.00	1	1	-	391735	                  Verminephrobacter eiseniae EF01-2
  0.00	3	0	F	506	            Alcaligenaceae
  0.00	2	0	G	517	              Bordetella
  0.00	1	1	S	520	                Bordetella pertussis
  0.00	1	1	S	123899	                Bordetella trematum
  0.00	1	0	G	222	              Achromobacter
  0.00	1	0	S	85698	                Achromobacter xylosoxidans
  0.00	1	1	-	762376	                  Achromobacter xylosoxidans A8
  0.00	1	0	F	75682	            Oxalobacteraceae
  0.00	1	0	G	963	              Herbaspirillum
  0.00	1	1	S	964	                Herbaspirillum seropedicae
  0.00	1	0	O	206389	          Rhodocyclales
  0.00	1	0	F	75787	            Rhodocyclaceae
  0.00	1	0	G	146937	              Azospira
  0.00	1	0	S	146939	                Azospira oryzae
  0.00	1	1	-	640081	                  Dechlorosoma suillum PS
  0.00	12	0	-	68525	        delta/epsilon subdivisions
  0.00	9	0	C	28221	          Deltaproteobacteria
  0.00	5	0	O	29	            Myxococcales
  0.00	3	0	-	80812	              Sorangiineae
  0.00	3	0	F	49	                Polyangiaceae
  0.00	3	0	G	39643	                  Sorangium
  0.00	3	0	S	56	                    Sorangium cellulosum
  0.00	3	3	-	1254432	                      Sorangium cellulosum So0157-2
  0.00	2	0	-	224462	              Nannocystineae
  0.00	2	0	F	224464	                Kofleriaceae
  0.00	2	0	G	162027	                  Haliangium
  0.00	2	0	S	80816	                    Haliangium ochraceum
  0.00	2	2	-	502025	                      Haliangium ochraceum DSM 14365
  0.00	2	0	O	69541	            Desulfuromonadales
  0.00	1	1	F	213421	              Desulfuromonadaceae
  0.00	1	0	F	213422	              Geobacteraceae
  0.00	1	0	G	28231	                Geobacter
  0.00	1	1	S	345632	                  Geobacter pickeringii
  0.00	2	0	O	213115	            Desulfovibrionales
  0.00	2	0	F	194924	              Desulfovibrionaceae
  0.00	2	0	G	872	                Desulfovibrio
  0.00	2	2	S	1716143	                  Desulfovibrio sp. J2
  0.00	3	0	C	29547	          Epsilonproteobacteria
  0.00	3	0	O	213849	            Campylobacterales
  0.00	2	0	F	72293	              Helicobacteraceae
  0.00	2	0	G	209	                Helicobacter
  0.00	2	0	S	138563	                  Helicobacter cetorum
  0.00	2	2	-	1163745	                    Helicobacter cetorum MIT 99-5656
  0.00	1	0	F	72294	              Campylobacteraceae
  0.00	1	0	G	194	                Campylobacter
  0.00	1	0	S	197	                  Campylobacter jejuni
  0.00	1	0	-	32022	                    Campylobacter jejuni subsp. jejuni
  0.00	1	1	-	354242	                      Campylobacter jejuni subsp. jejuni 81-176
  0.00	6	0	C	28211	        Alphaproteobacteria
  0.00	3	0	O	204441	          Rhodospirillales
  0.00	2	0	F	41295	            Rhodospirillaceae
  0.00	2	0	G	191	              Azospirillum
  0.00	2	0	S	193	                Azospirillum lipoferum
  0.00	1	1	-	137722	                  Azospirillum sp. B510
  0.00	1	1	-	862719	                  Azospirillum lipoferum 4B
  0.00	1	0	F	433	            Acetobacteraceae
  0.00	1	0	G	522	              Acidiphilium
  0.00	1	0	S	62140	                Acidiphilium multivorum
  0.00	1	1	-	926570	                  Acidiphilium multivorum AIU301
  0.00	1	0	O	356	          Rhizobiales
  0.00	1	0	F	45404	            Beijerinckiaceae
  0.00	1	0	G	28209	              Chelatococcus
  0.00	1	1	S	444444	                Chelatococcus daeguensis
  0.00	1	0	O	766	          Rickettsiales
  0.00	1	0	F	775	            Rickettsiaceae
  0.00	1	0	-	33988	              Rickettsieae
  0.00	1	0	G	780	                Rickettsia
  0.00	1	0	S	1129742	                  belli group
  0.00	1	0	S	33990	                    Rickettsia bellii
  0.00	1	1	-	391896	                      Rickettsia bellii OSU 85-389
  0.00	1	0	O	204455	          Rhodobacterales
  0.00	1	0	F	31989	            Rhodobacteraceae
  0.00	1	0	G	302485	              Phaeobacter
  0.00	1	1	S	60890	                Phaeobacter gallaeciensis
  0.00	12	0	-	1783270	      FCB group
  0.00	12	0	-	68336	        Bacteroidetes/Chlorobi group
  0.00	11	0	P	976	          Bacteroidetes
  0.00	7	0	C	117743	            Flavobacteriia
  0.00	7	0	O	200644	              Flavobacteriales
  0.00	6	0	F	49546	                Flavobacteriaceae
  0.00	5	0	G	104264	                  Cellulophaga
  0.00	5	5	S	979	                    Cellulophaga lytica
  0.00	1	0	G	1518147	                  Wenyingzhuangia
  0.00	1	1	S	1790137	                    Wenyingzhuangia fucanilytica
  0.00	1	0	F	39782	                Blattabacteriaceae
  0.00	1	0	G	34098	                  Blattabacterium
  0.00	1	0	S	164514	                    Blattabacterium punctulatus
  0.00	1	1	-	1075399	                      Blattabacterium sp. (Cryptocercus punctulatus) str. Cpu
  0.00	2	0	C	200643	            Bacteroidia
  0.00	2	0	O	171549	              Bacteroidales
  0.00	1	0	F	815	                Bacteroidaceae
  0.00	1	0	G	816	                  Bacteroides
  0.00	1	1	S	817	                    Bacteroides fragilis
  0.00	1	0	F	171552	                Prevotellaceae
  0.00	1	0	G	838	                  Prevotella
  0.00	1	0	S	28132	                    Prevotella melaninogenica
  0.00	1	1	-	553174	                      Prevotella melaninogenica ATCC 25845
  0.00	1	0	C	768503	            Cytophagia
  0.00	1	0	O	768507	              Cytophagales
  0.00	1	0	F	1853232	                Hymenobacteraceae
  0.00	1	0	G	1379908	                  Rufibacter
  0.00	1	1	S	1379909	                    Rufibacter sp. DG15C
  0.00	1	0	C	1937959	            Saprospiria
  0.00	1	0	O	1936988	              Saprospirales
  0.00	1	0	F	1937961	                Haliscomenobacteraceae
  0.00	1	0	G	2349	                  Haliscomenobacter
  0.00	1	0	S	2350	                    Haliscomenobacter hydrossis
  0.00	1	1	-	760192	                      Haliscomenobacter hydrossis DSM 1100
  0.00	1	0	P	1090	          Chlorobi
  0.00	1	0	C	191410	            Chlorobia
  0.00	1	0	O	191411	              Chlorobiales
  0.00	1	0	F	191412	                Chlorobiaceae
  0.00	1	0	G	256319	                  Chlorobaculum
  0.00	1	1	S	274537	                    Chlorobaculum limnaeum
  0.00	3	0	-	1783257	      PVC group
  0.00	3	0	P	203682	        Planctomycetes
  0.00	2	0	C	203683	          Planctomycetia
  0.00	2	0	O	112	            Planctomycetales
  0.00	2	0	F	126	              Planctomycetaceae
  0.00	1	0	G	1649480	                Planctopirus
  0.00	1	0	S	120	                  Planctopirus limnophila
  0.00	1	1	-	521674	                    Planctopirus limnophila DSM 3776
  0.00	1	0	G	1649490	                Rubinisphaera
  0.00	1	0	S	119	                  Rubinisphaera brasiliensis
  0.00	1	1	-	756272	                    Rubinisphaera brasiliensis DSM 5305
  0.00	1	0	C	666505	          Phycisphaerae
  0.00	1	0	O	666506	            Phycisphaerales
  0.00	1	0	F	666507	              Phycisphaeraceae
  0.00	1	0	G	666508	                Phycisphaera
  0.00	1	0	S	547188	                  Phycisphaera mikurensis
  0.00	1	1	-	1142394	                    Phycisphaera mikurensis NBRC 102666
  0.00	2	0	P	203691	      Spirochaetes
  0.00	2	0	C	203692	        Spirochaetia
  0.00	1	0	O	136	          Spirochaetales
  0.00	1	0	F	137	            Spirochaetaceae
  0.00	1	0	G	157	              Treponema
  0.00	1	0	S	88058	                Treponema primitia
  0.00	1	1	-	545694	                  Treponema primitia ZAS-2
  0.00	1	0	O	1643686	          Brachyspirales
  0.00	1	0	F	143786	            Brachyspiraceae
  0.00	1	0	G	29521	              Brachyspira
  0.00	1	0	S	52584	                Brachyspira pilosicoli
  0.00	1	1	-	1042417	                  Brachyspira pilosicoli P43/6/78
  0.00	1	0	P	32066	      Fusobacteria
  0.00	1	0	C	203490	        Fusobacteriia
  0.00	1	0	O	203491	          Fusobacteriales
  0.00	1	0	F	1129771	            Leptotrichiaceae
  0.00	1	0	G	32067	              Leptotrichia
  0.00	1	1	S	712357	                Leptotrichia sp. oral taxon 212
  0.00	1	0	P	200918	      Thermotogae
  0.00	1	0	C	188708	        Thermotogae
  0.00	1	0	O	2419	          Thermotogales
  0.00	1	0	F	1643950	            Fervidobacteriaceae
  0.00	1	0	G	2422	              Fervidobacterium
  0.00	1	0	S	2424	                Fervidobacterium nodosum
  0.00	1	1	-	381764	                  Fervidobacterium nodosum Rt17-B1
  0.00	13	0	D	2157	    Archaea
  0.00	13	0	P	28890	      Euryarchaeota
  0.00	10	0	C	224756	        Methanomicrobia
  0.00	10	0	O	94695	          Methanosarcinales
  0.00	10	0	F	2206	            Methanosarcinaceae
  0.00	10	0	G	2207	              Methanosarcina
  0.00	4	0	S	2208	                Methanosarcina barkeri
  0.00	3	3	-	1434109	                  Methanosarcina barkeri str. Wiesmoor
  0.00	1	1	-	269797	                  Methanosarcina barkeri str. Fusaro
  0.00	3	0	S	170861	                Methanosarcina lacustris
  0.00	3	3	-	1434111	                  Methanosarcina lacustris Z-7289
  0.00	2	1	S	2209	                Methanosarcina mazei
  0.00	1	1	-	1434115	                  Methanosarcina mazei SarPi
  0.00	1	1	S	38027	                Methanosarcina siciliae
  0.00	2	0	C	183939	        Methanococci
  0.00	2	0	O	2182	          Methanococcales
  0.00	2	0	F	2183	            Methanococcaceae
  0.00	2	0	G	155862	              Methanothermococcus
  0.00	2	0	S	155863	                Methanothermococcus okinawensis
  0.00	2	2	-	647113	                  Methanothermococcus okinawensis IH1
  0.00	1	0	C	183925	        Methanobacteria
  0.00	1	0	O	2158	          Methanobacteriales
  0.00	1	0	F	2159	            Methanobacteriaceae
  0.00	1	0	G	2172	              Methanobrevibacter
  0.00	1	1	S	294671	                Methanobrevibacter olleyae
  0.01	49	0	D	10239	  Viruses
  0.01	48	0	-	35237	    dsDNA viruses, no RNA stage
  0.01	43	0	F	10240	      Poxviridae
  0.01	42	0	-	10241	        Chordopoxvirinae
  0.01	41	0	G	10242	          Orthopoxvirus
  0.01	39	39	S	28871	            Taterapox virus
  0.00	2	2	S	10243	            Cowpox virus
  0.00	1	0	G	10270	          Leporipoxvirus
  0.00	1	1	S	10271	            Rabbit fibroma virus
  0.00	1	0	-	40069	        unclassified Poxviridae
  0.00	1	0	S	39444	          Cotia virus
  0.00	1	1	-	930275	            Cotia virus SPAn232
  0.00	3	0	-	51368	      unclassified dsDNA viruses
  0.00	2	2	S	1349409	        Pandoravirus dulcis
  0.00	1	1	S	1349410	        Pandoravirus salinus
  0.00	1	0	F	10501	      Phycodnaviridae
  0.00	1	0	G	346673	        Coccolithovirus
  0.00	1	1	S	181082	          Emiliania huxleyi virus 86
  0.00	1	0	O	548681	      Herpesvirales
  0.00	1	0	F	10292	        Herpesviridae
  0.00	1	0	-	10357	          Betaherpesvirinae
  0.00	1	0	G	10358	            Cytomegalovirus
  0.00	1	1	S	1535247	              Saimiriine betaherpesvirus 4
  0.00	1	0	-	35268	    Retro-transcribing viruses
  0.00	1	0	F	11632	      Retroviridae
  0.00	1	0	-	35276	        unclassified Retroviridae
  0.00	1	0	-	206037	          Human endogenous retroviruses
  0.00	1	0	S	45617	            Human endogenous retrovirus K
  0.00	1	1	-	166122	              Human endogenous retrovirus K113
