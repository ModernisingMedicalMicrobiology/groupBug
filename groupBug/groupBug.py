#!/usr/bin/env python3
import sys
import os
import pandas as pd
from ete3 import NCBITaxa
from argparse import ArgumentParser, SUPPRESS
import copy
ncbi = NCBITaxa()
headers=['percentage','reads','reads_taxon','taxon','taxid','name']

def getSpecies(taxes):
    l=ncbi.get_lineage(taxes)
    r=ncbi.get_rank(l)
    s = ['species', 'species group','species subgroup']
    try:
        species = {'species':i for i in l if r[i] in s}['species']
    except:
        return 
    taxid2name = ncbi.get_taxid_translator([species])
    return taxid2name[species]

def isSpecies(tax):
    try:
        l=ncbi.get_lineage(tax)
        r=ncbi.get_rank(l)
        if r[tax] == 'species':
            return True
        else:
            return False
    except:
        return False


class kreportStats:
    def __init__(self,opts):
        self.orderList=False
        self.renameDict=False
        self.domain=opts.domain
        self.saveName = opts.saveName
        if  opts.topNum != None:
            self.topNum = int(opts.topNum)
        else:
            self.topNum = opts.topNum
        self.suf = opts.suf
        self.descendants = ncbi.get_descendant_taxa(opts.domain,intermediate_nodes=True)
        self.maxReads=opts.maxReads
        self.barplot=opts.barplot

    def getDomain(self,kr):
        kr=kr
        sample=kr.split('/')[-1].replace(self.suf,'')#.rstrip('.fuge.kreport.txt') 
        df = pd.read_csv( kr,  sep='\t', names=headers )
        if self.renameDict:
            try:
                sample=self.renameDict[sample.replace(self.suf,'')]['newName']
            except:
                return None,False

        df['sample_name'] = sample.replace(self.suf,'') 
        df['ts'] = df['taxid'].apply(isSpecies)
        domain=df.loc[(df['taxid'].isin(self.descendants)) & ( df['taxon'] == 'S') & ( df['ts'] == True) ]
        #domain['ts'] = domain['taxid'].apply(isSpecies)
        #domain=domain.loc[df['ts'] == True]
        domain.drop(['ts'],inplace=True, axis=1)

        #domain=df.loc[(df['taxid'].isin(self.descendants)) & ( df['taxon'] == 'G') ]
        return domain,True

    def topN(self,df,n=5):
        tn=df.nlargest(n,'reads')
        sampleName=list(df['sample_name'])[0]
        tn.to_csv( '{0}_top_{1}_{2}_species.tsv'.format(sampleName, n, self.domain), sep='\t' )
        return tn

    def makePivot(self):
        df = self.all_samples
        p_all_samples = df.pivot(index='taxid',columns='sample_name',values='reads')

        # filter 
        p_all_samples['Total'] = p_all_samples.sum(axis=1)
        if self.topNum == None:
            self.topNum = 30
        p_all_samples =  p_all_samples.nlargest(self.topNum,'Total')
        p_all_samples.drop(['Total'],inplace=True, axis=1)
        p_all_samples['species'] = p_all_samples.index.map( getSpecies )
        #p_all_samples['species'] = p_all_samples.index.map( getGenus )
        p_all_samples = p_all_samples.fillna(0)
        p_all_samples.to_csv( '{0}_pivot.tsv'.format(self.domain), sep='\t' )
        self.p_all_samples=p_all_samples
 
    def visClutserMap(self):
        if self.saveName != None:
            import matplotlib as mpl
            mpl.use('Agg')
        import matplotlib.pyplot as plt
        from matplotlib.colors import LogNorm
        import seaborn as sns; sns.set(color_codes=True)
        df=self.p_all_samples
        df=df.drop(['species'],axis=1)
        df = df[df.columns].astype(int)
#        df.replace(0,0.1,inplace=True)

        # optional vis args
        if self.orderList:
            col_cluster = False
            df=df[self.orderList]
        else:
            col_cluster = True

        if self.maxReads:
            z_score = None
            vmax = int(self.maxReads)
            label = 'N reads'
        else:
            z_score = 1
            vmax = None
            label = 'Z score '
    
        g=sns.clustermap(df,
                    cmap="Blues",
                    metric='euclidean',
                    method='centroid',
                    z_score=z_score,
                    vmax=vmax,
                    xticklabels=True,
                    col_cluster=col_cluster,
                    yticklabels=self.p_all_samples['species'],
                    figsize=(12, 8),
                    cbar_kws={'label': label})
        plt.gcf().subplots_adjust(bottom=0.20,right=0.75,left=0,top=0.98)
        
        if self.barplot != None:
            dfT=df.T.reset_index()
            taxids=df.reset_index()['taxid'].to_list()
            dfT['Bacterial reads']=dfT[taxids].sum(axis=1)

            cax_col_dend_ax = g.ax_col_dendrogram.axes
            sns.barplot(x="sample_name", 
                    y="Bacterial reads", 
                    data=dfT, 
                    color='k',
                    ax=cax_col_dend_ax)
            cax_col_dend_ax.set_yscale("symlog")
            cax_col_dend_ax.axis('on')
#            cax_col_dend_ax.tick_params(axis='y', which='minor')
            cax_col_dend_ax.axes.get_xaxis().set_visible(False)


        if self.saveName != None:
            plt.savefig(self.saveName)
        else:
            plt.show()
            plt.clf()


    
    def processFiles(self, files):
        tables=[]
        for f in files:
            d,p=self.getDomain(f)
            if p==False:
                continue
            tables.append(d[['sample_name', 'reads', 'taxid', 'name' ]])
            if self.topNum != None:
                self.topN(d,int(self.topNum))
    
        all_samples = pd.concat( tables )
        all_samples['name'] = all_samples['name'].str.strip()
    
        all_samples.to_csv( '{0}_all_species.tsv'.format(self.domain), sep='\t' )
    
        all_samples['reads'] = all_samples['reads'].astype(int)
        self.all_samples=all_samples 
        
    def countTaxes(self,taxes):
        df=self.all_samples.loc[(self.all_samples['taxid'].isin(taxes))]
        df.to_csv( '{0}_specific_taxids.tsv'.format(self.domain), sep='\t' )
        df=df.pivot(index='taxid',columns='sample_name',values='reads')
        df['species'] = df.index.map( getSpecies )
        df = df.fillna(0)
        df.to_csv( '{0}_pivot_specific_taxids.tsv'.format(self.domain), sep='\t' )
        return df

    def getOrder(self,metafile,orderfile):
        df=pd.read_csv(metafile)
        df.set_index('oldName',inplace=True)
        self.renameDict = df.to_dict('index')
        o=open(orderfile,'rt').read()
        orderList=o.split('\n')
        self.orderList=list(filter(None,orderList))
        
    def run(self,opts):
        if opts.order != None:
            self.getOrder(opts.metafile,opts.order)
        self.processFiles(opts.kraken_reports)
        self.makePivot()
        self.visClutserMap()
        if opts.taxids != None:
             self.countTaxes(opts.taxids)
    
    
if __name__ == '__main__':
    parser = ArgumentParser(description='cluster heatmap and information from kraken reports')
    parser.add_argument('-k', '--kraken_reports', required=True, nargs='+',
                             help='list of kraken style report files')
    parser.add_argument('-d', '--domain', required=False, default='bacteria',
                             help='Domain of life to display, bacteria, viruses etc')
    parser.add_argument('-t', '--taxids', required=False,nargs='+',default=None,
                             help='list of taxids to specifically count')
    parser.add_argument('-sv', '--saveName', required=False, default=None,
                             help='file name to save plot as')
    parser.add_argument('-n', '--topNum', required=False, default=None, 
                             help='Number of discrete species to display')
    parser.add_argument('-suf', '--suf', required=False, default='',
                             help='suffix to delete from sample name')
    parser.add_argument('-meta', '--metafile', required=False,
                             help='csv list old and new sample names')
    parser.add_argument('-order', '--order', required=False,
                             help='csv list of x axis sample name orders')
    parser.add_argument('-m', '--maxReads', required=False, default=None, 
                             help='replaces maximum reads to show with ceiliing of N reads')
    parser.add_argument('-bar', '--barplot', required=False, action='store_false',
                             help='plot bar plot of total reads')


    opts, unknown_args = parser.parse_known_args()
    ks=kreportStats(opts)
    ks.run(opts)
